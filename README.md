# Netflix Clone using Flutter and Firebase

A new Flutter application.

## Key features:
Note: This project is mostly about UI development, some functionalities have not been implemented yet.

## Screenshots

<img src="/uploads/a8c1bfa71ae6c361457fbdf2c4a891ed/Screenshot_20210326-213702.png" width="400" height="600">
<img src="/uploads/8de2cf9b46a0039dba40b4aaa73eba4c/Screenshot_20210326-213720.png" width="400" height="600">
<img src="/uploads/9e1b907c3c20d869be6df8cef675fb0b/Screenshot_20210326-213730.png" width="400" height="600">
<img src="/uploads/bb914b7149f23140be866d73df98d4b5/Screenshot_20210326-213746.png" width="400" height="600">
