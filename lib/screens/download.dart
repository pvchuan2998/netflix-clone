import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Download extends StatefulWidget {
  @override
  _DownloadState createState() => _DownloadState();
}

class _DownloadState extends State<Download> {
  @override
  Widget build(BuildContext context) {
    final condition = 'ON';
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black45,
        title: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.info_outline_rounded,
                color: Colors.white,
              ),
            ),
            Text(
              'Smart Download',
              style: TextStyle(color: Colors.grey),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                condition,
                style: TextStyle(color: Colors.blue),
              ),
            )
          ],
        ),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/image/download.png'),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 10.0, right: 10.0),
                child: Text('Movies and TV shows that you download appear here.',
                    style: TextStyle(color: Colors.grey, fontSize: 22.0, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center),
              ),
              SizedBox(height: 130.0,),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0)
                ),
                child: MaterialButton(
                  onPressed: null,
                  child: Text('Find something to download',  style: TextStyle(color: Colors.black87, fontSize: 18.0)),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
