import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/widgets/video.dart';
import 'package:video_player/video_player.dart';

import '../main.dart';

class Detail extends StatefulWidget {
  final DocumentSnapshot info;
  Detail({this.info});
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 50.0),
                      child: SizedBox(
                        height: 150.0,
                        width: 250.0,
                        child: Image.network(widget.info.data['image']),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          widget.info.data['match'],
                          style: TextStyle(
                              color: Colors.green, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          widget.info.data['year'],
                          style: TextStyle(color: Colors.white),
                        ),
                        Container(
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(5.0)),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text(
                                widget.info.data['age'],
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                        Text(
                          widget.info.data['season'],
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: Container(
                        height: 35.0,
                        width: 330.0,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5.0)),
                        child: FlatButton.icon(
                            onPressed: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Videoplayer(videoData: widget.info.data['video'],)));
                            },
                            icon: Icon(
                              Icons.play_arrow,
                              color: Colors.black,
                              size: 27.0,
                            ),
                            label: Text(
                              'Play',
                              style: TextStyle(color: Colors.black87),
                            )),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Container(
                        height: 35.0,
                        width: 330.0,
                        decoration: BoxDecoration(
                            color: Colors.grey.shade500.withOpacity(0.4),
                            borderRadius: BorderRadius.circular(5.0)),
                        child: FlatButton.icon(
                            onPressed: null,
                            icon: Icon(
                              Icons.download_rounded,
                              color: Colors.white70,
                              size: 27.0,
                            ),
                            label: Text(
                              'Download S1:E1',
                              style: TextStyle(color: Colors.white),
                            )),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                          height: 70.0,
                          child: Text(
                            widget.info.data['info'],
                            style: TextStyle(
                              color: Colors.white70,
                            ),
                            textAlign: TextAlign.center,
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                          height: 50.0,
                          child: Text(
                            widget.info.data['starring'],
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        MaterialButton(
                          onPressed: null,
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.add,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text('My List',
                                    style: TextStyle(color: Colors.white)),
                              )
                            ],
                          ),
                        ),
                        MaterialButton(
                          onPressed: null,
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.thumb_up_alt_rounded,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text('Rate',
                                    style: TextStyle(color: Colors.white)),
                              )
                            ],
                          ),
                        ),
                        MaterialButton(
                          onPressed: null,
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.share_rounded,
                                color: Colors.white,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text('Share',
                                    style: TextStyle(color: Colors.white)),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    Divider(
                      color: Colors.grey.shade500,
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(widget.info.data['image']),
                        fit: BoxFit.fill,
                        colorFilter: ColorFilter.mode(
                            Colors.black87, BlendMode.darken))),
              ),
            ),
            expandedHeight: 515.0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => Homepage()));
              },
            ),
          )
        ],
      ),
    );
  }
}
