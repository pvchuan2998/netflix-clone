import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import 'detail.dart';

class Homescreen extends StatefulWidget {
  @override
  _HomescreenState createState() => _HomescreenState();
}

class _HomescreenState extends State<Homescreen> {
  @override
  Widget build(BuildContext context) {

    Future getMovieData(String collectionName) async {
      final firestore = Firestore.instance;
      QuerySnapshot snapshot =
      await firestore.collection(collectionName).getDocuments();
      return snapshot.documents;
    }

    routeToDetail(DocumentSnapshot info) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => Detail(info: info,)));
    }

    return Scaffold(
      backgroundColor: Colors.black,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.black,
            leading: Image.asset(
              'assets/image/n_symbol.png',
              scale: 20.0,
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: MaterialButton(
                  onPressed: () {},
                  child: Text(
                    'TV shows',
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: MaterialButton(
                  onPressed: () {},
                  child: Text(
                    'Movies',
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: MaterialButton(
                  onPressed: () {},
                  child: Text(
                    'My List',
                    style: TextStyle(color: Colors.white, fontSize: 18.0),
                  ),
                ),
              ),
            ],
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 250.0,
              child: Image.asset('assets/image/1.jpg'),
            ),
          ),
          SliverToBoxAdapter(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MaterialButton(
                  onPressed: null,
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.add,
                        size: 27.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(
                          'My list',
                          style: TextStyle(color: Colors.white, fontSize: 18.0),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5.0)),
                  child: FlatButton.icon(
                      onPressed: null,
                      icon: Icon(
                        Icons.play_circle_outline,
                        size: 32.0,
                        color: Colors.black,
                      ),
                      label: Text(
                        'Play',
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                      )),
                ),
                MaterialButton(
                  onPressed: null,
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.info_outline_rounded,
                        size: 27.0,
                        color: Colors.white,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(
                          'Info',
                          style: TextStyle(color: Colors.white, fontSize: 18.0),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 200.0,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, right: 120.0),
                    child: Text(
                      'Continue watching for you',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: SizedBox(
                          width: 100.0,
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                height: 100.0,
                                width: 100.0,
                                child: Image.asset('assets/image/1.jpg'),
                              ),
                              LinearProgressIndicator(value: 50.0),
                              Row(
                                children: <Widget>[
                                  IconButton(
                                      icon: Icon(
                                        Icons.info_outline_rounded,
                                        size: 25.0,
                                        color: Colors.white,
                                      ),
                                      onPressed: null),
                                  IconButton(
                                      icon: Icon(
                                        Icons.menu,
                                        size: 25.0,
                                        color: Colors.white,
                                      ),
                                      onPressed: null)
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      SizedBox(
                        width: 100.0,
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 100.0,
                              width: 100.0,
                              child: Image.asset('assets/image/2.jpg'),
                            ),
                            LinearProgressIndicator(value: 50.0),
                            Row(
                              children: <Widget>[
                                IconButton(
                                    icon: Icon(
                                      Icons.info_outline_rounded,
                                      size: 25.0,
                                      color: Colors.white,
                                    ),
                                    onPressed: null),
                                IconButton(
                                    icon: Icon(
                                      Icons.menu,
                                      size: 25.0,
                                      color: Colors.white,
                                    ),
                                    onPressed: null)
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 150.0,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 170.0),
                    child: Text('Popular on Netflix',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(
                    height: 120.0,
                    child: FutureBuilder(
                      future: getMovieData('popular'),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          //return Lottie.asset('assets/animations/loading.json');
                          return CircularProgressIndicator(
                            backgroundColor: Colors.white,
                          );
                        } else {
                          return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.length,
                            itemBuilder: (_, index) {
                              return GestureDetector(
                                onTap: () => routeToDetail(snapshot.data[index]),
                                //onTap: null,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SizedBox(
                                    height: 100.0,
                                    width: 150.0,
                                    child: Image.network(
                                        snapshot.data[index].data['image']),
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 150.0,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 230.0),
                    child: Text('Animated',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(
                    height: 120.0,
                    child: FutureBuilder(
                      future: getMovieData('animated'),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Lottie.asset('assets/animations/loading.json');
                        } else {
                          return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.length,
                            itemBuilder: (_, index) {
                              return GestureDetector(
                                onTap: null,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SizedBox(
                                    height: 100.0,
                                    width: 150.0,
                                    child: Image.network(
                                        snapshot.data[index].data['image']),
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 150.0,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 200.0),
                    child: Text('Netflix Original',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(
                    height: 120.0,
                    child: FutureBuilder(
                      future: getMovieData('originals'),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Lottie.asset('assets/animations/loading.json');
                        } else {
                          return ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: snapshot.data.length,
                            itemBuilder: (_, index) {
                              return GestureDetector(
                                onTap: null,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: SizedBox(
                                    height: 100.0,
                                    width: 150.0,
                                    child: Image.network(
                                        snapshot.data[index].data['image']),
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(
              height: 350.0,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 120.0),
                    child: Text(
                      'Available Now: Season 2',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 250.0,
                    width: 400.0,
                    child: Image.asset('assets/image/2.jpg'),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 3.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0)
                          ),
                          child: FlatButton.icon(
                            onPressed: null,
                            icon: Icon(Icons.play_arrow, color: Colors.black, size: 27.0,),
                            label: Text('Play', style: TextStyle(color: Colors.black, fontSize: 18.0),),
                          ),
                        ),
                        SizedBox(width: 50.0,),
                        Container(
                          child: FlatButton.icon(
                            onPressed: null,
                            icon: Icon(Icons.add, color: Colors.white, size: 27.0,),
                            label: Text('My List', style: TextStyle(color: Colors.white, fontSize: 18.0),),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );;
  }
}
