import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  Future getData() async {
    final firestore = Firestore.instance;
    QuerySnapshot querySnapshot = await firestore.collection('popular').getDocuments();
    return querySnapshot.documents;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Color(0xff525152),
        leading: Icon(
          Icons.search,
          color: Colors.white,
        ),
        title: TextField(
          decoration: InputDecoration(
              hintText: 'Search for a movie, show, genre etc.',
              hintStyle: TextStyle(color: Colors.white),
              suffixIcon: IconButton(
                onPressed: null,
                icon: Icon(
                  Icons.mic,
                  color: Colors.white,
                ),
              ),
              border: InputBorder.none),
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
              child: Text(
                'Popular Searches',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0,
                ),
              ),
            ),
            SizedBox(
              height: 450.0,
              child: FutureBuilder(
                future: getData(),
                builder: (context, snap) {
                  if(snap.connectionState == ConnectionState.waiting){
                    return Lottie.asset('assets/animations/loading.json');
                  }
                  else{
                    return ListView.builder(
                      itemCount: snap.data.length,
                      itemBuilder: (_, index) {
                        return Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: GestureDetector(
                            onTap: null,
                            child: Container(
                              color: Color(0xff323232),
                              child: SizedBox(
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 100.0,
                                      width: 150.0,
                                      child: Image.network(snap.data[index].data['image'], fit: BoxFit.fitHeight,),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 7.0),
                                      child: Container(
                                        width: 150.0,
                                        child: Text(snap.data[index].data['name'], style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                        ),),
                                      ),
                                    ),
                                    Icon(Icons.play_arrow, color: Colors.grey,)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
