import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/screens/commingsoon.dart';
import 'package:flutterapp/screens/detail.dart';
import 'package:flutterapp/screens/download.dart';
import 'package:flutterapp/screens/homescreen.dart';
import 'package:flutterapp/screens/more.dart';
import 'package:flutterapp/screens/search.dart';
import 'package:lottie/lottie.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple food app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Homepage(),
    );
  }
}

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {

    PageController pageController = PageController();
    void onTap(int pageValue) {
      setState(() {
        selectedIndex = pageValue;
      });
      pageController.jumpToPage(pageValue);
    }

    return Scaffold(
      backgroundColor: Colors.black,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this.selectedIndex,
        backgroundColor: Colors.black87,
        type: BottomNavigationBarType.fixed,
        selectedFontSize: 14.0,
        unselectedFontSize: 12.0,
        //selectedIconTheme: IconThemeData(color: Colors.black87),
        selectedIconTheme: IconThemeData (
            color: Colors.grey,
            size: 30
        ),
        unselectedIconTheme: IconThemeData (
            color: Colors.white,
            size: 25
        ),

        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text('Home',
                    style: TextStyle(color: Colors.white, fontSize: 12.0)),
              )),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text('Search',
                    style: TextStyle(color: Colors.white, fontSize: 12.0)),
              )),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.video_collection,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text('Comming soon',
                    style: TextStyle(color: Colors.white, fontSize: 12.0)),
              )),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.download_rounded,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text('Download',
                    style: TextStyle(color: Colors.white, fontSize: 12.0)),
              )),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.menu_rounded,
              ),
              title: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text('More',
                    style: TextStyle(color: Colors.white, fontSize: 12.0)),
              )),
        ],
        onTap: onTap,
      ),
      body: PageView(
        children: <Widget>[
          Homescreen(),
          Search(),
          CommingSoon(),
          Download(),
          More()
        ],
        controller: pageController,
        onPageChanged: (value) {
          setState(() {
            selectedIndex = value;
          });
        },
      ),
    );
  }
}
